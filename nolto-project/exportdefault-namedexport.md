## Named export - `index.js` 사용
> [참고자료](https://leedr0730.medium.com/%EB%94%94%EB%A0%89%ED%86%A0%EB%A6%AC-%ED%8C%8C%EC%9D%BC-export-%ED%95%98%EA%B8%B0-index-js-%EC%82%AC%EC%9A%A9-2e698a8e2cbd)
### 1. 기존 폴더 정리 이유
- 프로젝트 규모가 커지면서 직관적으로 찾아 쓸수 있게 하는 의도
- 엉키지 않고 개별성을 유지하면서 유기적으로 사용 되게 하기 위한 규칙 필요 >> `export` `default`
- ✔각 파일마다 해주면 무척 번거로울 수 있기에 `index.js` 파일 사용해 `re-export` 해줄수 있음!
    - React 모듈에 대한 `public interfaces` 효과적 제작 가능 

### 2. `Named export`
```javascript

// case 1
export { default as App } from "./App";
export { default as Home } from "./Home";
export { default as Login } from "./Login";
export { default as Navigation } from "./Navigation";
export { default as NotFound } from "./NotFound";
export { default as Signup } from "./Signup";

// case 2
import App from "./App";
import Home from "./Home";
import Login from "./Login";
import Navigation from "./Navigation";
import NotFound from "./NotFound";
import Signup from "./Signup";

export { App, Home, Login, Navigation, NotFound, Signup } ; ✔
import { App, Home, Login, Navigation, NotFound, Signup } from 'components'; ✔ 이렇게 한번에 가져올수 있다!

```




