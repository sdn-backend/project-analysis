## Spring Directory
> [참고자료](https://www.popit.kr/spring-guide-directory-%EA%B0%80%EC%9D%B4%EB%93%9C/)

### 1. 패키지 구성
> 크게 2가지 
#### 1.1 레이어 계층형
- 각 계층을 대표하는 디렉터리 기준으로 코드 구성
- ✔장점: 해당 프로젝트에 이해가 상대적으로 낮아도 전체적인 구조를 빠르게 파악할 수 있다는 점
- ✔단점: 디렉터리에 클래스들이 너무 많이 모이게 된다는 점
```
└── src
      ├── main
      │   ├── java
      │   │   └── com
      │   │       └── example
      │   │           └── demo
      │   │               ├── DemoApplication.java
      │   │               ├── config
      │   │               ├── controller
      │   │               ├── dao
      │   │               ├── domain
      │   │               ├── exception
      │   │               └── service
      │   └── resources
      │       └── application.properties
```
#### 1.2 도메인형
- 도메인 디렉터리 기준으로 코드 구성
- ✔장점: 관련된 코드들이 응집해 있는 점
- ✔단점: 프로젝트에 대한 이해도가 낮을 경우 전체적인 구조 파악 어려움
```
└── src
      ├── main
      │   ├── java
      │   │   └── com
      │   │       └── example
      │   │           └── demo
      │   │               ├── DemoApplication.java
      │   │               ├── coupon
      │   │               │   ├── controller
      │   │               │   ├── domain
      │   │               │   ├── exception
      │   │               │   ├── repository
      │   │               │   └── service
      │   │               ├── member
      │   │               │   ├── controller
      │   │               │   ├── domain
      │   │               │   ├── exception
      │   │               │   ├── repository
      │   │               │   └── service
      │   │               └── order
      │   │                   ├── controller
      │   │                   ├── domain
      │   │                   ├── exception
      │   │                   ├── repository
      │   │                   └── service
      │   └── resources
      │       └── application.properties
```

#### 1.3 추가
- ✔`domain`: 도메인을 담당하는 디렉터리
- ✔`global`: 전체적인 설정 관리
- ✔`infra` : 인프라스트럭처를 관리하는 infra 디렉터리
```
└── src
      ├── main
      │   ├── java
      │   │   └── com
      │   │       └── spring
      │   │           └── guide
      │   │               ├── ApiApp.java
      │   │               ├── SampleApi.java
      │   │               ├── domain
      │   │               │   ├── coupon
      │   │               │   │   ├── api
      │   │               │   │   ├── application
      │   │               │   │   ├── dao
      │   │               │   │   ├── domain
      │   │               │   │   ├── dto
      │   │               │   │   └── exception
      │   │               │   ├── member
      │   │               │   │   ├── api
      │   │               │   │   ├── application
      │   │               │   │   ├── dao
      │   │               │   │   ├── domain
      │   │               │   │   ├── dto
      │   │               │   │   └── exception
      │   │               │   └── model
      │   │               │       ├── Address.java
      │   │               │       ├── Email.java
      │   │               │       └── Name.java
      │   │               ├── global
      │   │               │   ├── common
      │   │               │   │   ├── request
      │   │               │   │   └── resonse
      │   │               │   ├── config
      │   │               │   │   ├── SwaggerConfig.java
      │   │               │   │   ├── properties
      │   │               │   │   ├── resttemplate
      │   │               │   │   └── security
      │   │               │   ├── error
      │   │               │   │   ├── ErrorResponse.java
      │   │               │   │   ├── GlobalExceptionHandler.java
      │   │               │   │   └── exception
      │   │               │   └── util
      │   │               └── infra
      │   │                   ├── email
      │   │                   └── sms
      │   │                       ├── AmazonSmsClient.java
      │   │                       ├── SmsClient.java
      │   │                       └── dto
      │   └── resources
      │       ├── application-dev.yml
      │       ├── application-local.yml
      │       ├── application-prod.yml
      │       └── application.yml
```
