## `JSX.Element` `ReactNode` `ReactElement`
> [참고자료](https://simsimjae.tistory.com/426)
- 📌타입스크립트 작성하다보면 타입을 지정해줘야 되는 부분에서 해당 개념을 알아야됨!


```javascript
import React, { Suspense } from 'react';

interface Props {
  fallback: React.ReactElement | null;
  children: React.ReactNode;
}

const SsrSuspense = ({ fallback, children }: Props) =>
  typeof window === 'undefined' ? (
    <>{children}</>
  ) : (
    <Suspense fallback={fallback}>{children}</Suspense>
  );

export default SsrSuspense;
```

### 1. 배경지식
- ✔클래스형 컴포넌트 반환 >> `ReactNode`
- ✔함수형 컴포넌트 반환 >> `ReactElement`
- ![image.png](./image.png)

#### 1.1 ReactElement
```typescript
interface ReactElement<P = any, T extends string | JSXElementConstructor<any> = string | JSXElementConstructor<any>> {
    type: T;
    props: P;
    key: Key | null;
}
```

#### 1.2 ReactNode
```typescript
type ReactText = string | number;
type ReactChild = ReactElement | ReactText;

interface ReactNodeArray extends Array<ReactNode> {}
type ReactFragment = {} | ReactNodeArray;

type ReactNode = ReactChild | ReactFragment | ReactPortal | boolean | null | undefined;
```

#### 1.3 JSX.Element
```typescript
declare global {
  namespace JSX {
    interface Element extends React.ReactElement<any, any> { }
  }
}
```
