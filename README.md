## Project list
- [2021-zzimkkong](https://gitlab.com/sdn-backend/project-analysis/-/blob/main/project/2021-zzimkkong.md)
- [2021-nolto](https://gitlab.com/sdn-backend/project-analysis/-/blob/main/react/2021-nolto.md)

## References
- [`Spring Directory` 가이드](https://gitlab.com/sdn-backend/project-analysis/-/blob/main/etc/spring-doc.md)
- [채팅 프로젝트 조사 `with Spring`](etc/채팅.md)
- [스크럼](etc/scrum.md)
