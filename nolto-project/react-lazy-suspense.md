## `React.lazy` `Suspense` 코드분할 `성능향상`
> [참고자료](https://doqtqu.tistory.com/349)

### 1. 코드분할
- 사용하지 않는 자바스크립트 줄이기
    - `React.lazy()` >> 자바스크립트 번들 분할가능!
- 앱커지면 번들파일도 커짐 ㅠ,ㅠ

### 1.1 React.lazy
> ⭐ `Suspense` 컴포넌트 하위에서 렌더링 진행되어야 함!! (반드시!!!!)
```javascript
import OtherComponent from './OtherComponent'; // 기존
const OtherComponent = React.lazy(() => import('./OtherComponent')); // ✔ 개선
```
- 결국에는 Suspense는 lazy 컴포넌트 로드되는 동안 로딩 화면 화면과 같은 예비 컨텐츠를 보여줄수 있게됨!! ✔



