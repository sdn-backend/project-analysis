## 2021-nolto
> [참고자료](https://github.com/woowacourse-teams/2021-nolto)
- ![image-4.png](./image-4.png)
- [`Export default` `Named export` - index.js 사용](nolto-project/exportdefault-namedexport.md)
- [`JSX.Element` `ReactNode` `ReactElement` 차이](nolto-project/reactnode-reactelement.md)
- [React.lazy 및 Suspense 사용한 코드분할 - `성능향상`](nolto-project/react-lazy-suspense.md)


|역할|사용 기술|
|----|--------|
|FE|`React` `Nodejs(SSR)` `TypeScript`|
|BE|`Spring`|
|Cloud(AWS)|`EC2` `Cloudfront` `S3`|
|DB|`MariaDB`|
|Server|`nginx`|
### Front-End
![image.png](./image.png)
### Back-End
|Folder|
|------|
|![image-1.png](./image-1.png)|
|![image-2.png](./image-2.png)|

### Architecture
![image-3.png](./image-3.png)
